package com.example.demo.Controller.model;

import java.math.BigDecimal;

public class Mobil {
    private String nama;
    private Integer jumlah;
    private BigDecimal harga;

    public Mobil(String nama) {
        System.out.print("ini adalah Constructor mobil dengan nama"+nama+"\n");
    }

    public Mobil (String nama, Integer jumlah, BigDecimal harga){
        System.out.print("ini adalah Constractor mobil"+nama+"\n"+
        "jumlah "+jumlah+"\n"+
        "harga "+harga+"\n") ;
    }

    public Mobil() {

    }

    public void nama() {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }
}
