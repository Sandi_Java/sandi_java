package com.example.demo.basic;

import com.example.demo.basic.Luas;

public class Volume extends Luas {
    public Integer tinggi;
    public Integer sisi;


    public Volume() {

    }

    public Volume(Integer panjang, Integer lebar, Integer tinggi, Integer sisi) {
        this.panjang = panjang;
        this.lebar = lebar;
        this.tinggi = tinggi;
        this.sisi = sisi;
    }


    public Integer hitungVolumeBalok() {
        Integer Volume = panjang * lebar * tinggi;

        return Volume;
    }
    public Integer hitungVolumeKubus() {
        Integer Volume = sisi * sisi * sisi;

        return Volume;
    }


    public Integer getTinggi() {
        return tinggi;
    }

    public void setTinggi(Integer tinggi) {
        this.tinggi = tinggi;
    }

    public Integer getSisi() {
        return sisi;
    }

    public void setSisi(Integer sisi) {
        this.sisi = sisi;
    }
}