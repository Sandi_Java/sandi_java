package com.example.demo.basic;

public class Luas {
    public Integer panjang;
    public Integer lebar;

    public Luas(Integer panjang, Integer lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public Luas() {

    }

    public Integer hitungLuas() {
        Integer luas = panjang * lebar;

        return luas;
    }

    public Integer getPanjang() {
        return panjang;
    }

    public void setPanjang(Integer panjang) {
        this.panjang = panjang;
    }

    public Integer getLebar() {
        return lebar;
    }

    public void setLebar(Integer lebar) {
        this.lebar = lebar;
    }
}