package com.example.demo.Controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@RequestMapping(value = "/bangunruang")
@Controller
public class BangunRuang {

    @RequestMapping(value = {"/",""} , method = RequestMethod.GET)
    public String index()
    {
        return "bangunruang/index";
    }

}
